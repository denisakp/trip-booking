<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>
        <link rel="stylesheet" href="{{ asset('dashboard/fonts/feather/feather.css')}}" />
        <link rel="stylesheet" href="{{ asset('dashboard/css/theme.min.css')}}" id="stylesheetLight">
        <link rel="stylesheet" href="{{ asset('dashboard/css/theme-dark.min.css')}}" id="stylesheetDark">
        @stack('styles')

        <style>
          body {
            display: none;
          }
        </style>
    </head>
    <body>
        @include('includes.dashboard.sidebar')

        <div class="main-content">
                <div class="header">
                    <div class="container-fluid">
                      <div class="header-body">
                        <div class="row align-items-end">
                          <div class="col">
                            <h6 class="header-pretitle">
                              Voyage
                            </h6>

                            <h1 class="header-title">
                              Tableau de bord
                            </h1>

                          </div>
                          <div class="col-auto">


                            {{-- <a href="#!" class="btn btn-primary lift">
                              Create Report
                            </a> --}}

                          </div>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="container-fluid">
                    @yield('content')
                </div>
        </div>

        <script src="{{ asset('dashboard/libs/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{ asset('dashboard/libs/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{ asset('dashboard/libs/autosize/dist/autosize.min.js')}}"></script>
        <script src="{{ asset('dashboard/js/theme.min.js')}}"></script>
        <script src="{{ asset('dashboard/js/dashkit.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js" integrity="sha512-lbwH47l/tPXJYG9AcFNoJaTMhGvYWhVM9YI43CT+uteTRRaiLCui8snIgyAN8XWgNjNhCqlAUdzZptso6OCoFQ==" crossorigin="anonymous"></script>
        @stack('scripts')

    </body>
</html>
