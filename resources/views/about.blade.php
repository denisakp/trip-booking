@extends('layouts.app')

@section('content')

<div class="body-inner">
    @include('includes.header')
	<!-- start Main Wrapper -->
    <div class="main-wrapper scrollspy-container">

        <section class="page-wrapper">

            <div class="container pt-100">

                <div class="section-title text-center w-100">
                    <h2><span><span>A</span> Propos de nous</span></h2>
                    <p>Nous ajoutons de la valeur à nos produits ce qui fait notre particularité </p>
                </div>

                <div class="two-column-css">

                    <p>LE TERTIAIRE est une entreprise spécialisée en ingénierie et conception informatique, créée en Août 2020, elle offre des services ingénierie logicielle; en administration système et réseau informatique. Elle compte au total 18 collaborateurs.</p>

                    <p>Comme axe de développement nous favorisons le développement personnel au sein de notre entreprise, nous rentrons dans une démarche d’innovation constante en ajoutant en permanence de la valeur dans tous nos produits et  services, en intégrant de nouvelles technologies dans les processus internes de l’entreprise.</p>

                    <p>Comme axe également nous optimisons la qualité du recrutement en misant beaucoup plus sur la passion sans oublier le développement de puissants systèmes de formations internes.</p>

                </div>

                <div class="mb-100"></div>

                <div class="row justify-content-center">

                    <div class="col-12 col-lg-11 col-xl-10">

                        <div class="counting-wrapper">

                            <div class="bg-image overlay-relative" style="background-image:url('images/image-bg/44.jpg');">

                                <div class="overlay-holder overlay-white opacity-8"></div>

                                <div class="row equal-height cols-1 cols-sm-3 cols-md-3 gap-30">

                                    <div class="col">

                                        <div class="item-counting">
                                            <div class="counting-inner">
                                                <div class="counting-number">
                                                    <span class="counter" data-decimal-delimiter="," data-thousand-delimiter="," data-value="2542" ></span>
                                                </div>
                                                <span class="counting-label">
                                                    Clients satisfaits</span>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col">

                                        <div class="item-counting">
                                            <div class="counting-inner">
                                                <div class="counting-number">
                                                    <span class="counter" data-decimal-delimiter="," data-thousand-delimiter="," data-value="896" ></span>
                                                </div>
                                                <span class="counting-label">
                                                    Forfaits touristiques</span>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col">

                                        <div class="item-counting">
                                            <div class="counting-inner">
                                                <div class="counting-number">
                                                    <span class="counter" data-decimal-delimiter="," data-thousand-delimiter="," data-value="365" ></span>
                                                </div>
                                                <span class="counting-label">Grands endroits</span>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="mb-100"></div>

                <div class="section-title text-center w-100">
                    <h2><span><span>Nos</span> caracteristiques</span></h2>
                    <p>Rentrer dans une démarche d'innovation constante en ajoutant de la valeur à nos produits</p>
                </div>

                <div class="row cols-1 cols-sm-2 cols-lg-3 gap-20 gap-md-40">

                    <div class="col">
                        <div class="featured-icon-horizontal-01 clearfix">
                            <div class="icon-font">
                                <i class="elegent-icon-gift_alt text-primary"></i>
                            </div>
                            <div class="content">
                                <h6>Nos meilleures offres</h6>
                                <p class="text-muted">Nous vous facilitons les déplacements avec de meilleures offres.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="featured-icon-horizontal-01 clearfix">
                            <div class="icon-font">
                                <i class="elegent-icon-wallet text-primary"></i>
                            </div>
                            <div class="content">
                                <h6>Meilleur prix garanti</h6>
                                <p class="text-muted">Vous ne devez pas toujours vous ruiner pour partir en voyage, soyez flexible dans vos choix. </p>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="featured-icon-horizontal-01 clearfix">
                            <div class="icon-font">
                                <i class="elegent-icon-heart_alt text-primary"></i>
                            </div>
                            <div class="content">
                                <h6>Nos passagers, notre métier</h6>
                                <p class="text-muted">Nos passagers nous font confiance, leur faire plaisir est notre priorité</p>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="featured-icon-horizontal-01 clearfix">
                            <div class="icon-font">
                                <i class="elegent-icon-heart_alt text-primary"></i>
                            </div>
                            <div class="content">
                                <h6>De la valeur ajoutée</h6>
                                <p class="text-muted">Nous optimisons la qualité du recrutement en misant beaucoup plus sur la passion et nous offrons de meilleures formations en internes à nos collaborateurs</p>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="featured-icon-horizontal-01 clearfix">
                            <div class="icon-font">
                                <i class="elegent-icon-gift_alt text-primary"></i>
                            </div>
                            <div class="content">
                                <h6>Axe de développement</h6>
                                <p class="text-muted">Nous insistons sur le développement personnel de chaque collaborateur</p>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="featured-icon-horizontal-01 clearfix">
                            <div class="icon-font">
                                <i class="elegent-icon-wallet text-primary"></i>
                            </div>
                            <div class="content">
                                <h6>Notre particularité</h6>
                                <p class="text-muted">La majeur partie de nos collaborateurs est jeune avec un très fort esprit d'innovation</p>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="mb-100"></div>

                <div class="section-title text-center w-100">
                    <h2><span><span>Notre</span> équipe</span></h2>
                    <p>La liste de nos collaborateurs</p>
                </div>

                <div class="row equal-height cols-2 cols-sm-3 cols-lg-4 gap-30 mb-40">

                    <div class="col">

                        <figure class="user-grid">

                            <div class="image">
                                <img src="assets/images/image-man/04.jpg" alt="Team" class="img-circle" />
                            </div>

                            <figcaption class="content">

                                <h6 class="text-uppercase">Robert Smith</h6>
                                <p>CEO</p>
                                <ul class="social">
                                    <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                </ul>

                            </figcaption>

                        </figure>

                    </div>

                    <div class="col">

                        <figure class="user-grid">

                            <div class="image">
                                <img src="assets/images/image-man/05.jpg" alt="Team" class="img-circle" />

                            </div>

                            <figcaption class="content">

                                <h6 class="text-uppercase">Raymond bright</h6>
                                <p>Marketing</p>
                                <ul class="social">
                                    <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                </ul>

                            </figcaption>

                        </figure>

                    </div>

                    <div class="col">

                        <figure class="user-grid">

                            <div class="image">
                                <img src="assets/images/image-man/03.jpg" alt="Team" class="img-circle" />
                            </div>

                            <figcaption class="content">

                                <h6 class="text-uppercase">Chaiyapatt Putsathit</h6>
                                <p>CEO
                                <ul class="social">
                                    <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                </ul>

                            </figcaption>

                        </figure>

                    </div>

                    <div class="col">

                        <figure class="user-grid">

                            <div class="image">
                                <img src="assets/images/image-man/02.jpg" alt="Team" class="img-circle" />
                            </div>

                            <figcaption class="content">

                                <h6 class="text-uppercase">Khairoz Nadzri</h6>
                                <p>Accounting</p>
                                <ul class="social">
                                    <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                </ul>

                            </figcaption>

                        </figure>

                    </div>

                    <div class="col">

                        <figure class="user-grid">

                            <div class="image">
                                <img src="assets/images/image-man/nia.jpg" alt="Team" class="img-circle" />
                            </div>

                            <figcaption class="content">

                                <h6 class="text-uppercase">Nia AJAVON</h6>
                                <p>Manager</p>
                                <ul class="social">
                                    <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                </ul>

                            </figcaption>

                        </figure>

                    </div>

                    <div class="col">

                        <figure class="user-grid">

                            <div class="image">
                                <img src="assets/images/image-man/04.jpg" alt="Team" class="img-circle" />
                            </div>

                            <figcaption class="content">

                                <h6 class="text-uppercase">Khairoz Nadzri</h6>
                                <p>Accounting</p>
                                <ul class="social">
                                    <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                </ul>

                            </figcaption>

                        </figure>

                    </div>

                    <div class="col">

                        <figure class="user-grid">

                            <div class="image">
                                <img src="assets/images/image-man/ni.jpg" alt="Team" class="img-circle" />
                            </div>

                            <figcaption class="content">

                                <h6 class="text-uppercase">Rosemonde ajavon</h6>
                                <p>Developper</p>
                                <ul class="social">
                                    <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                </ul>

                            </figcaption>

                        </figure>

                    </div>

                    <div class="col">

                        <figure class="user-grid">

                            <div class="image">
                                <img src="assets/images/image-man/01.jpg" alt="Team" class="img-circle" />
                            </div>

                            <figcaption class="content">

                                <h6 class="text-uppercase">DENIS AKPAGNONITE</h6>
                                <p>Manager</p>
                                <ul class="social">
                                    <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                </ul>

                            </figcaption>

                        </figure>

                    </div>

                </div>

            </div>

        </section>

        <section class="border-top pv-30">

            <div class="row gap-20 gap-lg-40 align-items-center justify-content-center line-1">
                <div class="col-auto">
                    <div class="image"><img src="assets/images/image-logo/logo-sm-bl-03.png" alt="image" /></div>
                </div>
                <div class="col-auto">
                    <div class="image"><img src="assets/images/image-logo/logo-sm-bl-04.png" alt="image" /></div>
                </div>
                <div class="col-auto">
                    <div class="image"><img src="assets/images/image-logo/logo-sm-bl-05.png" alt="image" /></div>
                </div>
                <div class="col-auto">
                    <div class="image"><img src="assets/images/image-logo/logo-sm-bl-07.png" alt="image" /></div>
                </div>
                <div class="col-auto">
                    <div class="image"><img src="assets/images/image-logo/logo-sm-bl-08.png" alt="image" /></div>
                </div>
            </div>

        </section>

    </div>
    <!-- end Main Wrapper -->

    @include('includes.footer')
</div>

@endsection

@push('scripts')
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
@endpush
