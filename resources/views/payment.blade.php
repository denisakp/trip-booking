@extends('layouts.app')

@section('content')

    <div class="body-inner">
        @include('includes.header')

		<!-- start Main Wrapper -->
		<div class="main-wrapper scrollspy-container">

			<section class="page-wrapper page-detail">

				<div class="container pt-30">

					<div class="row gap-20 gap-lg-40">

						<div class="col-12 col-lg-7">

							<div class="content-wrapper">

								<div class="form-draft-payment">

									<form>
										<h4 class="heading-title"><span>Finaliser le paiement <small class="font-sm text-muted">/ <i class="fas fa-lock"></i> sécurisé</small></span></h4>
										<p class="post-heading">Attention, ce formulaire est juste une simulation. Aucun frais ne sera prélevé de vos comptes.</p>

										<div class="row gap-15 mb-15">

											<div class="col-6 col-sm-3 col-md-4">

												<div class="form-group">
													<label>Nombre de Place</label>
													<input id="seat" type="number" min="1" value="1" class="form-control">
 												</div>
											</div>

											<div class="col-6 col-sm-3 col-md-4">

												<div class="form-group">
													<label>Poids total des bagages</label>
													<input id="weight" type="number" min="0" value="0" class="form-control">
 												</div>
											</div>

										</div>

										<div class="mb-20"></div>

										<div class="alert alert-info line-145 padding-30" role="alert">
											<h4 class="alert-heading line-125 mb-5">Merci!</h4>
											<p class="lead mb-10">d'utiliser notre plateforme pour votre voyage</p>
										</div>

										<div class="box-payment mt-20">

											<div class="row gap-20">

												<div class="col-6">

													<div class="payment-option-item">

														<div class="custom-control custom-radio">
															<input type="radio" id="BoxPayment-CreditCard" name="BoxPayment" class="custom-control-input" value="BoxPayment-formCreditCard" />
															<label class="custom-control-label text-muted" for="BoxPayment-CreditCard">
																<img class="d-inline-block" src="/assets/images/image-payment/payment-credit-cards.jpg" alt="images" /><br/>
																Payement par carte de credit
															</label>
														</div>

													</div>

												</div>

												<div class="col-6">

													<div class="payment-option-item">

														<div class="custom-control custom-radio">
															<input type="radio" id="BoxPayment-paypal" name="BoxPayment" class="custom-control-input" value="BoxPayment-formPaypal" />
															<label class="custom-control-label text-muted" for="BoxPayment-paypal">
																<img class="d-inline-block" src="/assets/images/image-payment/payment-paypal.jpg" alt="images" /><br/>
																Payement par Paypal
															</label>
														</div>

													</div>

												</div>

											</div>

											<div id="BoxPayment-formCreditCard" class="payment-form">

												<h5>Total à débiter: <span id="fprice" class="text text-success">{{$trip->price * 1}} XOF</span></h5>
											</div>

										</div>

										<hr class="mv-40">

										<div class="custom-control custom-checkbox">
											<input id="formDraftPayment02-terms" name="formDraftPayment02-terms" type="checkbox" class="custom-control-input" value="paymentsCreditCard"/>
											<label class="custom-control-label" for="formDraftPayment02-terms">En envoyant ce formulaire, vous consentez à nos <a href="#">termes et conditions</a> ainsi que <a href="#"> notre politique de retour de fonds</a> and  <a href="#"></a> .</label>
										</div>

										<div class="row mt-20">

											<div class="col-sm-8 col-md-6">

												<button type="button" onclick="checkout()" class="btn btn-primary">Soumettre la reservation</button>

											</div>

										</div>

									</form>

								</div>

							</div>

						</div>

						<div class="col-12 col-lg-5">

							<aside class="sticky-kit sidebar-wrapper no-border">

								<div class="booking-box">

									<div class="box-heading"><h3 class="h6 text-white text-uppercase">Récapitulatif	</h3></div>
									<div class="box-content">

										<a href="#" class="tour-small-grid-01 mb-20 clearfix">

											<div class="image"><img src="{{ asset('assets/images/back.png') }}" alt="images" /></div>
											<div class="content">
												<h6>{{$trip->departure->name}} - {{$trip->destination->name}}</h6>
												<span class="price">Tarif: <span class="h6 line-1 text-primary number">{{$trip->price}}</span> FCFA</span>
												<span class="price">Durée du trajet: : <span class="h6 line-1 text-primary number">{{$trip->travel_time}}</span> heure(s)</span>
											</div>

										</a>

										<span class="font600 text-muted line-125">Date de départ</span>
										<h4 class="line-125 choosen-date mt-3"><i class="ri-calendar"></i> {{$trip->departure_date}}</h4>

										<ul class="border-top mt-20 pt-15">
											<li class="clearfix">Nombre de sièges<span class="float-right" id="seatN">1</span></li>
											<li class="clearfix pl-15">Montant<span class="float-right" id="priceN">{{$trip->price}}</span></li>
											<li class="clearfix">Poids des bagages<span class="float-right" id="weightN">0 Kg</span></li>
											<li class="clearfix pl-15">Frais d'exès de poids <span class="float-right" id="weghtFees">0</span></li>
											<li class="clearfix">Frais d'assurance<span class="float-right">500</span></li>
											<li class="clearfix border-top font700 text-uppercase">
												<div class="border-top mt-1">
												<span>Total</span><span class="float-right text-success" id="finalPrice">{{$trip->price * 1}} CFA</span>
												</div>
											</li>
										</ul>

									</div>

									<div class="box-bottom bg-light">
										<h6 class="font-sm">Avez-vous des questions ?</h6>
										<p class="font-sm">Contactez nous sur les numéros <span class="text-primary">+22890919293 / 97989900</span>.</p>
									</div>

								</div>

							</aside>

						</div>

					</div>

				</div>

			</section>

		</div>
		<!-- end Main Wrapper -->

        @include('includes.footer')

    </div>

    @include('includes.auth-modal')
@endsection

@push('scripts')
	<script type="text/javascript">
		let seat = document.getElementById('seat')
		let weight = document.getElementById('weight')
		let thePrice = 0
		let fees = 0
		let assurance = 500
		let total = 0

		seat.addEventListener('change', function(event) {
			event.preventDefault()
			var price = {{$trip->price}}*parseInt(seat.value)
			document.getElementById('seatN').innerText = seat.value
			document.getElementById('priceN').innerText = price
			thePrice = parseInt(price)
			total = thePrice+fees+assurance
			document.getElementById('finalPrice').innerText = parseInt(total)+' XOF'
			document.getElementById('fprice').innerText = parseInt(total)+' XOF'
		})

		weight.addEventListener('change', function(event) {
			event.preventDefault()
			document.getElementById('weightN').innerText = weight.value+'Kg'
			if(parseInt(weight.value) > 10){
				fees = 250*(weight.value - 10)
				document.getElementById('weghtFees').innerText = fees
				total = thePrice+fees+assurance
				document.getElementById('finalPrice').innerText = parseInt(total)+' XOF'
				document.getElementById('fprice').innerText = parseInt(total)+' XOF'
			}else {
				document.getElementById('weghtFees').innerText = 0
				total = thePrice+assurance
				document.getElementById('finalPrice').innerText = parseInt(total)+' XOF'
				document.getElementById('fprice').innerText = parseInt(total)+' XOF'
			}
		})

		function checkout() {
			return alert('Traitement du processus de paiement enclenché')
		}

	</script>
@endpush
