@extends('layouts.dashboard')

@section('content')
<div class="row">
    <div class="col-12 col-lg-10">
      <div class="card">

        <div class="card-header">
            <div class="row align-items-center">
                <div class="col">
                <h4 class="card-header-title">
                    Liste des trajets
                </h4>
                </div>

            </div>

        </div>

        <div class="table-responsive mb-0" 
            data-list="{&quot;valueNames&quot;: [&quot;currency-label&quot;, 
                &quot;currency-symbol&quot;, &quot;currency-price&quot;, &quot;currency-owner&quot;,
                &quot;currency-address&quot;, &quot;currency-available&quot;]}">

          <table id="_overview" class="table table-bordered table-sm table-nowrap card-table">
            
            <thead>
              <tr>
                <th scope="col">
                  <a href="#" class="text-muted list-sort" data-sort="currency-label">
                    ID
                  </a>
                </th>

                <th width="5%" >
                  <a href="#" class="text-muted list-sort" data-sort="currency-symbol">
                    Ville de départ
                  </a>
                </th>

                <th width="5%" >
                  <a href="#" class="text-muted list-sort" data-sort="currency-price">
                    Date de départ
                  </a>
                </th>

                <th><a href="#" class="text-muted list-sort" data-sort="currency-owner">
                    Nombre de place
                  </a>
                </th>

                <th><a href="#" class="text-muted list-sort" data-sort="currency-address">
                    Durée du trajet
                  </a>
                </th>

                <th><a href="#" class="text-muted list-sort" data-sort="currency-address">
                    Destination
                  </a>
                </th>

              </tr>
            </thead>

            <tbody class="list">
                @foreach ($index as $item)
                <tr>
                    <td class="currency-label">
                      {{ $item->id }}
                    </td>
    
                    <td class="currency-symbol">
                      {{ $item->departure->name }}
                    </td>
    
                    <td class="currency-price">
                      {{ $item->departure_date }}
                    </td>

                    <td class="currency-first_address">
                        {{ $item->seat }}
                    </td>
    
                    <td class="currency-first_address">
                        {{ $item->travel_time }}
                    </td>

                    <td class="text-right">
                        {{ $item->destination->name }}
                    </td>
    
                </tr>
                @endforeach
            </tbody>

          </table>
        </div>

      </div>

    </div>
</div>
@endsection

@push('styles')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
@endpush

@push('scripts')
    <script src="//cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#_overview').DataTable({
                responsive: true,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/French.json"
                }
            });
        } );
    </script>
@endpush
