@extends('layouts.app')

@section('content')

    <div class="body-inner">
        @include('includes.header')

        <!-- start Main Wrapper -->
		<div class="main-wrapper scrollspy-container">

			<section class="page-wrapper page-result pb-0">

				<div class="page-title bg-light mb-0">

					<div class="container">

						<div class="row gap-15 align-items-center">

							<div class="col-12 col-md-7">

								<nav aria-label="breadcrumb">
									<ol class="breadcrumb">
										<li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
										<li class="breadcrumb-item"><a href="#">Page</a></li>
									</ol>
								</nav>

								<h4 class="mt-0 line-125">Faite le tour du Togo</h4>

							</div>

						</div>

					</div>

				</div>

				<div class="container">

					<div class="row equal-height gap-30 gap-lg-40">

						<div class="col-12 col-lg-8">

							<div class="content-wrapper pv">

								<div class="d-flex justify-content-between flex-row align-items-center sort-group page-result-01">

								</div>

								<div class="tour-long-item-wrapper-01">
									{{-- @foreach ($trips as $item) --}}
									<figure class="tour-long-item-01">

										<a href="#">

											<div class="d-flex flex-column flex-sm-row align-items-xl-center">

												<div>
													<div class="image">
														<img src="{{ asset('assets/images/image-regular/tsev.jpeg') }}" alt="images" />
													</div>
												</div>

												<div>
													<figcaption class="content">
														<h5>Lomé - Tsévié</h5>
														<ul class="item-meta">

															<li>
																<div class="rating-item rating-sm rating-inline clearfix">
																	<div class="rating-icons">
																		<input type="hidden" class="rating" data-filled="rating-icon ri-star rating-rated" data-empty="rating-icon ri-star-empty" data-fractions="2" data-readonly value="4.5"/>
																	</div>
																	<p class="rating-text font600 text-muted font-12 letter-spacing-1">26 commentaires</p>
																</div>
															</li>
														</ul>
														<p> Passez de bon moment en  decouvrant la ville de Tsévié </p>
														<ul class="item-meta mt-15">
															<li>
																Départ: <span class="font600 h6 line-1 mv-0"> Lomé</span> - Destination: <span class="font600 h6 line-1 mv-0"> Tsévié</span>
															</li>
														</ul>
														<p class="mt-3 mb-0"> Tarif: <span class="h6 line-1 text-primary font16">2000FCFA</span> <span class="text-muted mr-5"></span></p>
													</figcaption>
												</div>

											</div>

										</a>

									</figure>

									<figure class="tour-long-item-01">

										<a href="#">

											<div class="d-flex flex-column flex-sm-row align-items-xl-center">

												<div>
													<div class="image">
														<img src="{{ asset('assets/images/image-regular/not.jpeg') }}" alt="images" />
													</div>
												</div>

												<div>

													<figcaption class="content">
														<h5>Lomé - Notsè</h5>
														<ul class="item-meta">

															<li>
																<div class="rating-item rating-sm rating-inline clearfix">
																	<div class="rating-icons">
																		<input type="hidden" class="rating" data-filled="rating-icon ri-star rating-rated" data-empty="rating-icon ri-star-empty" data-fractions="2" data-readonly value="4.5"/>
																	</div>
																	<p class="rating-text font600 text-muted font-12 letter-spacing-1">53 commentaires</p>
																</div>
															</li>
														</ul>
														<p> Découvrez la ville historique du peuple éwé </p>
														<ul class="item-meta mt-15">

															<li>
																Départ: <span class="font600 h6 line-1 mv-0"> Lomé</span> - Destination: <span class="font600 h6 line-1 mv-0"> Notsè</span>
															</li>
														</ul>
														<p class="mt-3">Tarif: <span class="h6 line-1 text-primary font16">3000FCFA</span> <span class="text-muted mr-5"></span></p>
													</figcaption>

												</div>

											</div>

										</a>

									</figure>

									<figure class="tour-long-item-01">

										<a href="#">

											<div class="d-flex flex-column flex-sm-row align-items-xl-center">

												<div>
													<div class="image">
														<img src="{{ asset('assets/images/image-regular/ata.jpeg') }}" alt="images" />
													</div>
												</div>

												<div>

													<figcaption class="content">
														<h5>Lomé - Atakpamé</h5>
														<ul class="item-meta">

															<li>
																<div class="rating-item rating-sm rating-inline clearfix">
																	<div class="rating-icons">
																		<input type="hidden" class="rating" data-filled="rating-icon ri-star rating-rated" data-empty="rating-icon ri-star-empty" data-fractions="2" data-readonly value="4.5"/>
																	</div>
																	<p class="rating-text font600 text-muted font-12 letter-spacing-2">44 Ccommentaires</p>
																</div>
															</li>
														</ul>
														<p>La cinquième ville du Togo de part sa population, découvrez ses collines verdoyantes qui lui donnent un petit charme</p>
														<ul class="item-meta mt-15">

															<li>
																Départ: <span class="font600 h6 line-1 mv-0"> Lomé</span> - Destination: <span class="font600 h6 line-1 mv-0"> Atakpamé</span>
															</li>
														</ul>
														<p class="mt-3">Tarif: <span class="h6 line-1 text-primary font16">4000FCFA</span> <span class="text-muted mr-5"></span></p>
													</figcaption>

												</div>

											</div>

										</a>

									</figure>

									<figure class="tour-long-item-01">

										<a href="#">

											<div class="d-flex flex-column flex-sm-row align-items-xl-center">

												<div>
													<div class="image">
														<img src="{{ asset('assets/images/image-regular/blit.jpeg') }}" alt="images" />
													</div>
												</div>

												<div>

													<figcaption class="content">
														<h5>Lomé - Blitta</h5>
														<ul class="item-meta">

															<li>
																<div class="rating-item rating-sm rating-inline clearfix">
																	<div class="rating-icons">
																		<input type="hidden" class="rating" data-filled="rating-icon ri-star rating-rated" data-empty="rating-icon ri-star-empty" data-fractions="2" data-readonly value="4.5"/>
																	</div>
																	<p class="rating-text font600 text-muted font-12 letter-spacing-2">44 commentaires</p>
																</div>
															</li>
														</ul>
														<p>Allez à la découverte de la ville de Blitta qui est à la fois une ville et une préfecture du Togo</p>
														<ul class="item-meta mt-15">

															<li>
																Départ: <span class="font600 h6 line-1 mv-0"> Lomé</span> - Destination: <span class="font600 h6 line-1 mv-0"> Blitta</span>
															</li>
														</ul>
														<p class="mt-3">Tarif: <span class="h6 line-1 text-primary font16">4500FCFA</span> <span class="text-muted mr-5"></span></p>
													</figcaption>

												</div>

											</div>

										</a>

									</figure>

									<figure class="tour-long-item-01">

										<a href="#">

											<div class="d-flex flex-column flex-sm-row align-items-xl-center">

												<div>
													<div class="image">
														<img src="{{ asset('assets/images/image-regular/sok.jpeg') }}" alt="images" />
													</div>
												</div>

												<div>

													<figcaption class="content">
														<h5>Lomé - Sokodé</h5>
														<ul class="item-meta">

															<li>
																<div class="rating-item rating-sm rating-inline clearfix">
																	<div class="rating-icons">
																		<input type="hidden" class="rating" data-filled="rating-icon ri-star rating-rated" data-empty="rating-icon ri-star-empty" data-fractions="2" data-readonly value="4.5"/>
																	</div>
																	<p class="rating-text font600 text-muted font-12 letter-spacing-2">44 commentaires</p>
																</div>
															</li>
														</ul>
														<p>Sokodé est une ville togolaise,considéré comme la deuxième du pays. Découvrez ses différentes cultures et sa population</p>
														<ul class="item-meta mt-15">

															<li>
																Départ: <span class="font600 h6 line-1 mv-0"> Lomé</span> - Destiantion: <span class="font600 h6 line-1 mv-0"> Sokodé</span>
															</li>
														</ul>
														<p class="mt-3">Tarif: <span class="h6 line-1 text-primary font16">4800FCFA</span> <span class="text-muted mr-5"></span></p>
													</figcaption>

												</div>

											</div>

										</a>

									</figure>

									<figure class="tour-long-item-01">

										<a href="#">

											<div class="d-flex flex-column flex-sm-row align-items-xl-center">

												<div>
													<div class="image">
														<img src="{{ asset('assets/images/image-regular/kara.jpeg') }}" alt="images" />
													</div>
												</div>

												<div>

													<figcaption class="content">
														<h5>Lomé - Kara</h5>
														<ul class="item-meta">

															<li>
																<div class="rating-item rating-sm rating-inline clearfix">
																	<div class="rating-icons">
																		<input type="hidden" class="rating" data-filled="rating-icon ri-star rating-rated" data-empty="rating-icon ri-star-empty" data-fractions="2" data-readonly value="4.5"/>
																	</div>
																	<p class="rating-text font600 text-muted font-12 letter-spacing-2">44 commmentaires</p>
																</div>
															</li>
														</ul>
														<p> Découvrez le chef-lieu de la region de la Kara et ses villes alentours</p>
														<ul class="item-meta mt-15">


																Départ/Destination: <span class="font600 h6 line-1 mv-0"> Lomé - Kara</span>

														</ul>
														<p class="mt-3">Tarif: <span class="h6 line-1 text-primary font16">5500FCFA</span> <span class="text-muted mr-5"></span></p>
													</figcaption>
												</div>

											</div>

										</a>

									</figure>

								</div>

								<div class="pager-wrappper mt-40">


								</div>

							</div>

						</div>

					</div>

				</div>

			</section>

		</div>
		<!-- end Main Wrapper -->

        @include('includes.footer')
    </div>

    @include('includes.auth-modal')

@endsection
