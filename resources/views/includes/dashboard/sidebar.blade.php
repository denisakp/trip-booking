<nav class="navbar navbar-vertical navbar-light fixed-left navbar-expand-md " id="sidebar">
    <div class="container-fluid">

      <!-- Toggler -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidebarCollapse" aria-controls="sidebarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Brand -->
      <a class="navbar-brand" href="/">
        <img src="{{ asset('dashboard/img/logo.svg')}}" class="navbar-brand-img 
        mx-auto" alt="...">
      </a>

      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidebarCollapse">
          <hr class="navbar-divider my-3">

          <h6 class="navbar-heading">
            Administration
          </h6>

          <!-- Navigation -->
          <ul class="navbar-nav">

            <li class="nav-item">
              <a class="nav-link" href="#trajetSide" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="trajetSide">
                <i class="fe fe-box"></i> Trajets
              </a>
              <div class="collapse" id="trajetSide">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="{{ route('trip.index')}} " class="nav-link active">
                      Vue d'ensemble
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="{{ route('trip.create')}}" class="nav-link ">
                      Ajouter
                    </a>
                  </li>

                </ul>
              </div>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="#reservationSide" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="reservationSide">
                <i class="fe fe-clipboard"></i> Reservations
              </a>
              <div class="collapse" id="reservationSide">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="/" class="nav-link active">
                      Vue d'ensemble
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="/" class="nav-link ">
                      Ajouter
                    </a>
                  </li>

                </ul>
              </div>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="{{ route('client.index') }}">
                <i class="fe fe-users"></i> Clients
              </a>
            </li>



          </ul>

          <hr class="navbar-divider my-3">

          <!-- Heading -->
          <h6 class="navbar-heading">
            Mon compte
          </h6>
  
          <!-- Navigation -->
          <ul class="navbar-nav mb-md-4">
            <li class="nav-item">
              <a class="nav-link " href="/">
                <i class="fe fe-user"></i> Profil
              </a>
            </li>
          </ul>
  
          <!-- Push content down -->
          <div class="mt-auto"></div>
  
          <!-- Customize -->
          <div title="Make Dashkit Your Own!" data-content="Switch the demo to Dark Mode or adjust the navigation layout, icons, and colors!">
            <a href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout').submit();" class="btn btn-block btn-primary mb-4">
              <i class="fe fe-log-out mr-2"></i> Déconnexion
            </a>
            <form id="logout" action="{{ url('/logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>
          </div>
            


      </div>
    </div>
  </nav>