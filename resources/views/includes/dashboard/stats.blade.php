<div class="row">
    <div class="col-12 col-lg-6 col-xl">
      <!-- Value  -->
      <div class="card">
        <div class="card-body">
          <div class="row align-items-center">
            <div class="col">

              <!-- Title -->
              <h6 class="text-uppercase text-muted mb-2">
                Reservations
              </h6>

              <!-- Heading -->
              <span class="h2 mb-0">
                {{ count($booking)}}
              </span>

            </div>

            <div class="col-auto">
              <!-- Icon -->
              <span class="h2 fe fe-calendar text-muted mb-0"></span>
            </div>
          </div> <!-- / .row -->
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-6 col-xl">
      <!-- Hours -->
      <div class="card">
        <div class="card-body">
          <div class="row align-items-center">
            <div class="col">

              <!-- Title -->
              <h6 class="text-uppercase text-muted mb-2">
                Trajets
              </h6>

              <!-- Heading -->
              <span class="h2 mb-0">
               0
              </span>

            </div>
            <div class="col-auto">

              <!-- Icon -->
              <span class="h2 fe fe-navigation text-muted mb-0"></span>

            </div>
          </div> <!-- / .row -->
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-6 col-xl">
      <!-- Exit -->
      <div class="card">
        <div class="card-body">
          <div class="row align-items-center">
            <div class="col">
              <!-- Title -->
              <h6 class="text-uppercase text-muted mb-2">
                Clients
              </h6>
              <!-- Heading -->
              <span class="h2 mb-0">
                0
              </span>
            </div>

            <div class="col-auto">

                <!-- Icon -->
                <span class="h2 fe fe-users text-muted mb-0"></span>
  
              </div>
          </div> <!-- / .row -->
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-6 col-xl">
      <!-- Time -->
      <div class="card">
        <div class="card-body">
          <div class="row align-items-center">
            <div class="col">
              <!-- Title -->
              <h6 class="text-uppercase text-muted mb-2">
               Caisse
              </h6>
              <!-- Heading -->
              <span class="h2 mb-0">
                0 FCFA
              </span>
            </div>
        
            <div class="col-auto">
              <!-- Icon -->
              <span class="h2 fe fe-dollar-sign text-muted mb-0"></span>
            </div>
          </div> <!-- / .row -->
        </div>
      </div>

    </div>
  </div>

  {{-- Ligne  deux --}}

  <div class="row">
    <div class="col-12 col-lg-6 col-xl">
      <!-- Value  -->
      <div class="card">
        <div class="card-body">
          <div class="row align-items-center">
            <div class="col">

              <!-- Title -->
              <h6 class="text-uppercase text-muted mb-2">
                Reservations
              </h6>

              <!-- Heading -->
              <span class="h2 mb-0">
                {{ count($booking)}}
              </span>

            </div>

            <div class="col-auto">
              <!-- Icon -->
              <span class="h2 fe fe-calendar text-muted mb-0"></span>
            </div>
          </div> <!-- / .row -->
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-6 col-xl">
      <!-- Hours -->
      <div class="card">
        <div class="card-body">
          <div class="row align-items-center">
            <div class="col">

              <!-- Title -->
              <h6 class="text-uppercase text-muted mb-2">
                Trajets
              </h6>

              <!-- Heading -->
              <span class="h2 mb-0">
               0
              </span>

            </div>
            <div class="col-auto">

              <!-- Icon -->
              <span class="h2 fe fe-navigation text-muted mb-0"></span>

            </div>
          </div> <!-- / .row -->
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-6 col-xl">
      <!-- Exit -->
      <div class="card">
        <div class="card-body">
          <div class="row align-items-center">
            <div class="col">
              <!-- Title -->
              <h6 class="text-uppercase text-muted mb-2">
                Clients
              </h6>
              <!-- Heading -->
              <span class="h2 mb-0">
                0
              </span>
            </div>

            <div class="col-auto">

                <!-- Icon -->
                <span class="h2 fe fe-users text-muted mb-0"></span>
  
              </div>
          </div> <!-- / .row -->
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-6 col-xl">
      <!-- Time -->
      <div class="card">
        <div class="card-body">
          <div class="row align-items-center">
            <div class="col">
              <!-- Title -->
              <h6 class="text-uppercase text-muted mb-2">
               Caisse
              </h6>
              <!-- Heading -->
              <span class="h2 mb-0">
                0 FCFA
              </span>
            </div>
        
            <div class="col-auto">
              <!-- Icon -->
              <span class="h2 fe fe-dollar-sign text-muted mb-0"></span>
            </div>
          </div> <!-- / .row -->
        </div>
      </div>

    </div>
  </div>