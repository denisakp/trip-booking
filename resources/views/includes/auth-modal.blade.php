<!-- start Login modal -->
<div class="modal fade modal-with-tabs form-login-modal" id="loginFormTabInModal" aria-labelledby="modalWIthTabsLabel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content shadow-lg">

            <nav class="d-none">
                <ul class="nav external-link-navs clearfix">
                    <li><a class="active" data-toggle="tab" href="#loginFormTabInModal-login">Authentification</a></li>
                    <li><a data-toggle="tab" href="#loginFormTabInModal-register">Inscription </a></li>
                    <li><a data-toggle="tab" href="#loginFormTabInModal-forgot-pass">Mot de passe oublié </a></li>
                </ul>
            </nav>

            <div class="tab-content">

                <div role="tabpanel" class="tab-pane active" id="loginFormTabInModal-login">

                    <div class="form-login">

                        <div class="form-header">
                            <h4>Bienvenu(e) sur TripCheap</h4>
                            <p>Veuillez vous connecter pour utiliser notre plateforme</p>
                        </div>

                        <div class="form-body">

                            <form method="post" action="{{ route('login') }}">
                                @csrf
                                <div class="d-flex flex-column flex-lg-row align-items-stretch">

                                    <div class="flex-md-grow-1 bg-primary-light">

                                        <div class="form-inner">
                                            <div class="form-group">
                                                <label>Adresse email</label>
                                                <input type="email" class="form-control" name="email" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Mot de passe</label>
                                                <input type="password" class="form-control" name="password" required />
                                            </div>
                                            <div class="d-flex flex-column flex-md-row mt-25">
                                                <div class="flex-shrink-0">
                                                    <button class="btn btn-primary btn-wide" type="submit">Se connecter</button>
                                                </div>
                                                <div class="ml-0 ml-md-15 mt-15 mt-md-0">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" name="remember" id="loginFormTabInModal-rememberMe">
                                                        <label class="custom-control-label" for="loginFormTabInModal-rememberMe">Rester connecté</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#loginFormTabInModal-forgot-pass" class="tab-external-link block mt-25 font600">Mot de passe oublié?</a>
                                        </div>

                                    </div>

                                </div>

                            </form>

                        </div>

                        <div class="form-footer">
                            <p>Pas encore membre ?<a href="#loginFormTabInModal-register" class="tab-external-link font600">Créer un comptep</a> gratuitement</p>
                        </div>

                    </div>

                </div>

                <div role="tabpanel" class="tab-pane fade in" id="loginFormTabInModal-register">

                    <div class="form-login">

                        <div class="form-header">
                            <h4>Créez un compte gratuitement</h4>
                            <p>Accédez à nos offre les plus exceptionnelles sur le marché</p>
                        </div>

                        <div class="form-body">

                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="d-flex flex-column flex-lg-row align-items-stretch">

                                    <div class="flex-grow-1 bg-primary-light">

                                        <div class="form-inner">
                                            <div class="form-group">
                                                <label>Nom complet</label>
                                                <input type="text" class="form-control" name="name" />
                                            </div>
                                            <div class="form-group">
                                                <label>Adresse Email</label>
                                                <input type="email" name="email" class="form-control" />
                                            </div>
                                            <div class="row cols-2 gap-10">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label>Mot de passe</label>
                                                        <input type="password" name="password" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label>Confirmez le mot de passe</label>
                                                        <input type="password" name="password_confirmation" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>

                                <div class="d-flex flex-column flex-md-row mt-30 mt-lg-10">
                                    <div class="flex-shrink-0">
                                        <button class="btn btn-primary btn-wide mt-5" type="submit">Inscription</button>
                                    </div>

                                </div>

                            </form>

                        </div>

                        <div class="form-footer">
                            <p>Vou êtes déjà membre ? <a href="#loginFormTabInModal-login" class="tab-external-link font600">Connectez vous</a></p>
                        </div>

                    </div>

                </div>

                <div role="tabpanel" class="tab-pane fade in" id="loginFormTabInModal-forgot-pass">

                    <div class="form-login">

                        <div class="form-header">
                            <h4>Mot de passe perdu?</h4>
                            <p>Veuillez fournir votre email</p>
                        </div>

                        <div class="form-body">
                            <form method="post" action="{{ route('password.request') }}">
                                @csrf
                                <p class="line-145">Nous vous enverrons les instructions à suivre pour récupéré votre mot de passe</p>
                                <div class="row">
                                    <div class="col-12 col-md-10 col-lg-8">
                                        <div class="form-group">
                                            <input type="email" name="email" class="form-control" placeholder="Adresse email" />
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-primary mt-5" type="submit">Récupérer mon compte</button>
                            </form>
                        </div>

                        <div class="form-footer">
                            <p>Plutôt<a href="#loginFormTabInModal-login" class="tab-external-link font600">se connecter</a> ou <a href="#loginFormTabInModal-register" class="tab-external-link font600">s'inscrire</a></p>
                        </div>

                    </div>

                </div>

            </div>

            <div class="text-center pb-20">
                <button type="button" class="close" data-dismiss="modal" aria-labelledby="Close">
                    <span aria-hidden="true"><i class="far fa-times-circle"></i></span>
                </button>
            </div>
        </div>
    </div>
</div>
<!-- end Login modal -->
