<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TownSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('towns')->insert([
            'name' => 'TSEVIE',
            'country_id' => 1
        ]);

        DB::table('towns')->insert([
            'name' => 'LOME',
            'country_id' => 1
        ]);

        DB::table('towns')->insert([
            'name' => 'KARA',
            'country_id' => 1
        ]);

        DB::table('towns')->insert([
            'name' => 'MANGO',
            'country_id' => 1
        ]);

        DB::table('towns')->insert([
            'name' => 'Dapaong',
            'country_id' => 1
        ]);

        DB::table('towns')->insert([
            'name' => 'NOTSE',
            'country_id' => 1
        ]);


        DB::table('towns')->insert([
            'name' => 'ATAKPAME',
            'country_id' => 1
        ]);

        DB::table('towns')->insert([
            'name' => 'BLITTA',
            'country_id' => 1
        ]);

        DB::table('towns')->insert([
            'name' => 'SOKODE',
            'country_id' => 1
        ]);

        DB::table('towns')->insert([
            'name' => 'KARA',
            'country_id' => 1
        ]);

        DB::table('towns')->insert([
            'name' => 'KANTE',
            'country_id' => 1
        ]);

        DB::table('towns')->insert([
            'name' => 'MANGO',
            'country_id' => 1
        ]);

        DB::table('towns')->insert([
            'name' => 'DAPAONG',
            'country_id' => 1
        ]);

        DB::table('towns')->insert([
            'name' => 'CINKASSE',
            'country_id' => 1
        ]);
    }
}
