<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->string('trip_code'); // Il s'agit d'une valeur générée lorsque le paiement est effectif
            $table->foreignId('user_id')->constrained(); // ID de l'utilisateur reserve son billet
            $table->foreignId('trip_id')->constrained(); // ID du trajet
            $table->unsignedInteger('seat'); // Renseigne le nombre de siège
            $table->unsignedInteger('amount'); 
            $table->boolean('is_done')->default(false); // Reférence si la reservation est déjà utilisée
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
